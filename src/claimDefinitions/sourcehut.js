/*
Copyright 2021 Yarmo Mackenbach

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const E = require('../enums')

const reURI = /^https:\/\/git\.sr\.ht\/~(.*)\/(.*)\/blob\/sourcehut_proof/

const processURI = (uri) => {
  const match = uri.match(reURI)

  const userName = match[1]
  const repoName = match[2]

  return {
    serviceprovider: {
      type: 'web',
      name: 'sourcehut'
    },
    match: {
      regularExpression: reURI,
      isAmbiguous: true
    },
    profile: {
      display: match[1],
      uri: `https://sr.ht/~${userName}`,
      qr: null
    },
    proof: {
      uri: uri,
      request: {
        fetcher: E.Fetcher.HTTP,
        access: E.ProofAccess.GENERIC,
        format: E.ProofFormat.TEXT,
        data: {
          url: `https://sr.ht/~${userName}/${repoName}/blob/sourcehut_proof`,
          format: E.ProofFormat.TEXT
        }
      }
    },
    claim: {
      format: E.ClaimFormat.MESSAGE,
      relation: E.ClaimRelation.EQUALS,
      path: ['description']
    }
  }
}

const tests = [
  {
    uri: 'https://git.sr.ht/~alice/randProj/blob/sourcehut_proof',
    shouldMatch: true
  },
  {
    uri: 'https://git.sr.ht/~alice/randProj/blob/sourcehut_proof/',
    shouldMatch: true
  }
]

exports.reURI = reURI
exports.processURI = processURI
exports.tests = tests
