/*
Copyright 2021 Yarmo Mackenbach

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
const list = [
  'dns',
  'irc',
  'xmpp',
  'matrix',
  'twitter',
  'reddit',
  'liberapay',
  'lichess',
  'hackernews',
  'lobsters',
  'devto',
  'gitea',
  'gitlab',
  'github',
  'sourcehut',
  'mastodon',
  'fediverse',
  'discourse',
  'owncast'
]

const data = {
  dns: require('./dns'),
  irc: require('./irc'),
  xmpp: require('./xmpp'),
  matrix: require('./matrix'),
  twitter: require('./twitter'),
  reddit: require('./reddit'),
  liberapay: require('./liberapay'),
  lichess: require('./lichess'),
  hackernews: require('./hackernews'),
  lobsters: require('./lobsters'),
  devto: require('./devto'),
  gitea: require('./gitea'),
  gitlab: require('./gitlab'),
  github: require('./github'),
  sourcehut: require('./sourcehut'),
  mastodon: require('./mastodon'),
  fediverse: require('./fediverse'),
  discourse: require('./discourse'),
  owncast: require('./owncast')
}

exports.list = list
exports.data = data
